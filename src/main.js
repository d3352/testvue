import Vue from "vue";
import App from "./App.vue";
import * as Sentry from "@sentry/vue";
import { Integrations } from "@sentry/tracing";

Vue.config.productionTip = false;

Sentry.init({
  Vue,
  dsn: "https://920446753a1e41d384b59055c6341573@o1073542.ingest.sentry.io/6073204",
  integrations: [
    new Integrations.BrowserTracing({
      tracingOrigins: ["localhost", "my-custom-vue-app.herokuapp.com/", /^\//],
    }),
  ],
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

new Vue({
  render: (h) => h(App),
}).$mount("#app");
